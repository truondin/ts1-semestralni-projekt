package cz.cvut.fel.pjv.game.model.enemies;

import cz.cvut.fel.pjv.game.controller.Handler;
import cz.cvut.fel.pjv.game.model.Player;
import cz.cvut.fel.pjv.game.view.Gfx;
import javafx.embed.swing.JFXPanel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RangeEnemyTest {
    private JFXPanel panel = new JFXPanel();
    private Handler handler;
    private RangeEnemy enemy;

    @BeforeEach
    public void setup(){
        handler = new Handler();
        enemy = new RangeEnemy(100,100, Gfx.CLOSE_ENEMY,handler);
        handler.addSprite(enemy);
    }

    @AfterEach
    public void clear(){
        handler = null;
        enemy = null;
    }

    @Test
    public void followTarget_mocksPlayerForEnemyToFollow_enemysVelXandVelYareTowardsPlayer(){
        // arrange and mock player as target
        Player player = mock(Player.class);
        when(player.getX()).thenReturn(0.0);
        when(player.getY()).thenReturn(0.0);
        enemy.setTarget(player);
        int expectedVelX = -enemy.getSpeed();
        int expectedVelY = -enemy.getSpeed();

        // act
        enemy.followTarget();

        // assert
        assertEquals(expectedVelX, enemy.getVelX(), "x");
        assertEquals(expectedVelY, enemy.getVelY());
    }

}