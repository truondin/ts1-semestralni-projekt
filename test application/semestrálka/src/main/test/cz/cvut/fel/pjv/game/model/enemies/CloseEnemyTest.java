package cz.cvut.fel.pjv.game.model.enemies;

import cz.cvut.fel.pjv.game.controller.Handler;
import cz.cvut.fel.pjv.game.model.Player;
import cz.cvut.fel.pjv.game.view.Gfx;
import javafx.embed.swing.JFXPanel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CloseEnemyTest {
    private JFXPanel panel = new JFXPanel();
    private Handler handler;
    private CloseEnemy enemy;

    @BeforeEach
    public void setup(){
        handler = new Handler();
        enemy = new CloseEnemy(100,100, Gfx.CLOSE_ENEMY,handler);
        handler.addSprite(enemy);
    }

    @AfterEach
    public void clear(){
        handler = null;
        enemy = null;
    }

    @Test
    public void move_targetIsInEnemysAttackRange_enemyStopsAndAttacks(){
        // arrange and mock player as target
        Player player = mock(Player.class);
        when(player.getX()).thenReturn(101D);
        when(player.getY()).thenReturn(101D);
        when(player.getHitbox()).thenReturn(enemy.getAttackRange());
        enemy.setTarget(player);
        enemy.setHasSpotted(true);
        int expectedVel = 0;
        int handlerCount = handler.getSprites().size();

        // act
        enemy.move();

        // assert
        assertEquals(expectedVel, enemy.getVelX());
        assertEquals(expectedVel, enemy.getVelY());
        assertTrue(handlerCount < handler.getSprites().size()); // check if enemy has created attacks
    }

    @Test
    public void followTarget_mocksPlayerForEnemyToFollow_enemysVelXandVelYareTowardsPlayer(){
        // arrange and mock player as target
        Player player = mock(Player.class);
        when(player.getX()).thenReturn(150D);
        when(player.getY()).thenReturn(150D);
        enemy.setTarget(player);
        int expectedVelX = enemy.getSpeed();
        int expectedVelY = enemy.getSpeed();

        // act
        enemy.followTarget();

        // assert
        assertEquals(expectedVelX, enemy.getVelX());
        assertEquals(expectedVelY, enemy.getVelY());
    }

}