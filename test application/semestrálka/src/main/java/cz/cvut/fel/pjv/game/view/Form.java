package cz.cvut.fel.pjv.game.view;

import cz.cvut.fel.pjv.game.controller.Music;
import cz.cvut.fel.pjv.game.view.state.Menu;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Objects;


public class Form {
    private Stage stage;
    private StackPane root;
    private Scene form;
    private final MediaPlayer mp = new MediaPlayer(Objects.requireNonNull(Music.MENU.getMusic()));
    private boolean isValid;
    private Label error;

    private TextField age;
    private ChoiceBox choice;
    private ChoiceBox diff;
    private Label diffValue;

    public Form(Stage stage) {
        this.stage = stage;
    }

    private void init(){
        root = new StackPane();
        form = new Scene(root);

        stage.setTitle("Avatar");
        stage.setWidth(900);
        stage.setHeight(600);
        stage.setResizable(false);

        ImageView img = new ImageView(Gfx.BACKGROUND_MENU.getImage());
        root.getChildren().add(img);

        mp.setVolume(0.5);
        mp.setCycleCount(MediaPlayer.INDEFINITE);
        mp.play();

        Image image = Gfx.CURSOR.getImage();
        form.setCursor(new ImageCursor(image));
    }

    private HBox createAgeBox(){
        HBox hboxAge = new HBox(5);
        Label ageLabel= new Label("Age?");
        ageLabel.setFont(Font.loadFont("file:src/main/resources/fonts/font.ttf",15));
        age = new TextField();
        hboxAge.setAlignment(Pos.CENTER);
        hboxAge.getChildren().add(ageLabel);
        hboxAge.getChildren().add(age);

        return hboxAge;
    }

    private boolean validateAge(String ageStr){
        try {
            int age = Integer.parseInt(ageStr);
            if (age <= 0 ){
                error.setText("Invalid age!");
                return false;
            }else if(age < 15){
                error.setText("too young!");
                return false;
            }else if (age > 100){
                error.setText("too old!");
                return false;
            }else{
                return true;
            }

        }catch (NumberFormatException e){
            error.setText("Invalid format!");
        } return false;
    }

    private VBox createChoice(){
        VBox boxChoice = new VBox(5);
        Label choiceLabel = new Label("Do you know Avatar? ");
        choiceLabel.setFont(Font.loadFont("file:src/main/resources/fonts/font.ttf",15));
        choice = new ChoiceBox(FXCollections.observableArrayList("YES", "NO"));

        boxChoice.getChildren().add(choiceLabel);
        boxChoice.getChildren().add(choice);
        boxChoice.setAlignment(Pos.CENTER);
        return boxChoice;
    }

    private boolean validateChoice(){
        try{
            String choiceValue = choice.getValue().toString();
            if (choiceValue.equals("NO")){
                error.setText("You dont deserve to play!");
                return false;
            }else if(choiceValue.equals("YES")){
                return true;
            }
        }catch (NullPointerException e){
            error.setText("Choice was not picked!");
        }
        return false;
    }

    private HBox createDiff(){
        HBox hboxDifficulty = new HBox(5);
        Label difficultyLabel = new Label("Difficulty? ");
        difficultyLabel.setFont(Font.loadFont("file:src/main/resources/fonts/font.ttf",15));
        diff = new ChoiceBox(
                FXCollections.observableArrayList("Easy", "Medium", "Hard")
        );

        hboxDifficulty.getChildren().add(difficultyLabel);
        hboxDifficulty.getChildren().add(diff);
        hboxDifficulty.setAlignment(Pos.CENTER);
        return hboxDifficulty;
    }

    private boolean validateDiff(){
        try{
            diffValue.setText("Difficulty set on " + diff.getValue().toString());
            return true;
        }catch (NullPointerException e){
            error.setText("Choose difficulty!");
        }return false;
    }

    private void setValid(){
        isValid =  validateAge(age.getText()) && validateChoice() && validateDiff();
    }

    private void createForm(){
        Font font = Font.loadFont("file:src/main/resources/fonts/font.ttf",12);
        Label label = new Label("Can you play this game ? ");
        label.setFont(font);
        error = new Label("");
        error.setFont(font);
        diffValue = new Label();
        diffValue.setFont(font);
        Button submit = new Button(" start ");
        submit.setFont(font);
        submit.setOnAction(actionEvent -> {
            setValid();
            if (isValid){
                startGame();
            }
        });


        VBox box = new VBox(10);

        HBox hboxAge = createAgeBox();
        VBox hboxChoice = createChoice();
        HBox hboxDifficulty = createDiff();

        box.getChildren().add(label);
        box.getChildren().add(hboxDifficulty);
        box.getChildren().add(hboxAge);
        box.getChildren().add(hboxChoice);

        box.getChildren().add(submit);
        box.getChildren().add(diffValue);
        box.getChildren().add(error);

        diff.setOnAction(actionEvent -> validateDiff());
        age.setOnAction(actionEvent -> validateAge(age.getText()));
        choice.setOnAction(actionEvent -> validateChoice());

        box.setAlignment(Pos.CENTER);
        root.getChildren().add(box);
    }

    public void startForm(){
        init();
        createForm();
        stage.setScene(form);
        stage.show();
    }

    public void startGame(){
        Menu menu = new Menu(stage);
        menu.start();
    }
}
