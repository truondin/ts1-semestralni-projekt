package cz.cvut.fel.pjv.game.model.attacks;

import cz.cvut.fel.pjv.game.controller.Handler;
import cz.cvut.fel.pjv.game.model.Player;
import cz.cvut.fel.pjv.game.model.enemies.CloseEnemy;
import cz.cvut.fel.pjv.game.view.Gfx;
import javafx.embed.swing.JFXPanel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class EarthTest {
    private JFXPanel panel = new JFXPanel();
    private Handler handler;
    private Earth earth;

    @BeforeEach
    public void setup(){
        handler = new Handler();
        earth = new Earth(100, 100, Gfx.EARTH_ATTACK, handler, 120, 120);
        handler.addSprite(earth);
    }

    @AfterEach
    public void clear(){
        handler = null;
        earth = null;
    }

    @Test
    public void dealDamage_earthAttackEffectsEnemy_enemysHealthDecreasedAndIsSlowed(){
        // Arrange
        Player player = mock(Player.class);
        CloseEnemy enemy = new CloseEnemy(100,100,Gfx.CLOSE_ENEMY,handler);
        enemy.setTarget(player);
        enemy.setVelX(4);
        enemy.setVelY(2);
        handler.addSprite(enemy);
        int expectedHealth = enemy.health - earth.getDamage();
        int expectedHandlerSize = handler.getSprites().size() -1;
        float expectedVelX = enemy.getVelX() / 2;
        float expectedVelY = enemy.getVelY() / 2;

        //Act
        earth.dealDamage(enemy);
        enemy.update();

        // Assert
        assertEquals(expectedHealth, enemy.health);
        assertTrue(enemy.isSlowed());
        assertEquals(expectedVelX, enemy.getVelX());
        assertEquals(expectedVelY, enemy.getVelY());
        assertEquals(expectedHandlerSize, handler.getSprites().size());
    }
}