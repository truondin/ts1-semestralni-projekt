package cz.cvut.fel.pjv.game.model.attacks;

import cz.cvut.fel.pjv.game.controller.Handler;
import cz.cvut.fel.pjv.game.model.Player;
import cz.cvut.fel.pjv.game.model.enemies.CloseEnemy;
import cz.cvut.fel.pjv.game.model.enemies.RangeEnemy;
import cz.cvut.fel.pjv.game.view.Gfx;
import javafx.embed.swing.JFXPanel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class FireTest {
    private JFXPanel panel = new JFXPanel();
    private Handler handler;
    private Fire fire;

    @BeforeEach
    public void setup(){
        handler = new Handler();
        fire = new Fire(100, 100, Gfx.FIRE_ATTACK, handler, 120, 120);
        handler.addSprite(fire);
    }

    @AfterEach
    public void clear(){
        handler = null;
        fire = null;
    }

    @Test
    public void dealDamage_fireAttackDamagesCloseEnemy_closeEnemyHasDecreasedHealthAndIsBurnt(){
        // Arrange and mocks player as target
        Player player = mock(Player.class);
        CloseEnemy enemy = new CloseEnemy(100,100,Gfx.CLOSE_ENEMY,handler);
        enemy.setTarget(player);

        handler.addSprite(enemy);
        int enemy_health = enemy.health - fire.getDamage();

        // Act
        fire.dealDamage(enemy);

        // Assert
        assertEquals(enemy_health, enemy.health);
        for (int i = 0; i < 10; i++) { // for loop for burning assert
            assertTrue(enemy.isBurnt());
            enemy.update();
            enemy_health -= 1;
            assertEquals(enemy_health , enemy.health);
        }
        assertFalse(enemy.isBurnt());

    }

    @Test
    public void dealDamage_fireAttackDamagesRangeEnemy_rangeEnemyHasDecreasedHealthAndIsBurnt(){
        // assert and mocks player as target
        Player player = mock(Player.class);
        RangeEnemy enemy = new RangeEnemy(100,100,Gfx.RANGE_ENEMY,handler);
        enemy.setTarget(player);
        handler.addSprite(enemy);
        int enemy2_health = enemy.health - fire.getDamage();

        // Act
        fire.dealDamage(enemy);

        // Assert
        assertEquals(enemy2_health , enemy.health);
        for (int i = 0; i < 10; i++) {
            assertTrue(enemy.isBurnt());

            enemy.update();

            enemy2_health -= 1;

            assertEquals(enemy2_health , enemy.health);
        }

        assertFalse(enemy.isBurnt());

    }

}