package cz.cvut.fel.pjv.game.view.state;

import cz.cvut.fel.pjv.game.controller.Camera;
import cz.cvut.fel.pjv.game.controller.Handler;
import cz.cvut.fel.pjv.game.controller.PlayerControl;
import cz.cvut.fel.pjv.game.model.Player;
import cz.cvut.fel.pjv.game.model.attacks.AttackType;
import cz.cvut.fel.pjv.game.model.attacks.EnemyAttack;
import cz.cvut.fel.pjv.game.model.enemies.CloseEnemy;
import cz.cvut.fel.pjv.game.model.objects.*;
import cz.cvut.fel.pjv.game.view.Gfx;
import javafx.embed.swing.JFXPanel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class GameTest {

    private JFXPanel panel = new JFXPanel();
    private Handler handler;

    @BeforeEach
    public void setup() {
        handler = new Handler();
    }

    @AfterEach
    public void clear() {

        handler = null;
    }

    @Test
    public void killEnemyWithFire() {
        Camera camera = mock(Camera.class);
        Player player = new Player(0, 0, Gfx.PLAYER, handler);
        handler.addSprite(player);
        CloseEnemy enemy = new CloseEnemy(5, 5, Gfx.CLOSE_ENEMY, handler);
        enemy.setTarget(player);
        handler.addSprite(enemy);

        // player changes attack to fire
        while (player.getAttack() != AttackType.FIRE) {
            player.changeAttack();
        }
        assertSame(AttackType.FIRE, player.getAttack());

        // attack enemy
        while (handler.getSprites().contains(enemy)) {
            player.attack((int) enemy.getX(), (int) enemy.getY());
            handler.update(camera);
            if (player.fireAmmo == 0) {
                player.changeAttack();
                assertNotSame(player.getAttack(), AttackType.FIRE);
            }
        }
        assertTrue(enemy.isDead());
        assertFalse(handler.getSprites().contains(enemy));
    }

    @Test
    public void playerHeals() {
        Player player = new Player(0, 0, Gfx.PLAYER, handler);
        handler.addSprite(player);

        //dealing damage to player
        EnemyAttack attack = new EnemyAttack(0, 0, Gfx.ENEMY_ATTACK, handler, player.velX, player.y, 5);
        attack.dealDamage(player);
        assertTrue(player.health < 100);

        // new heart spawned
        Heart heart = new Heart(0, 0, Gfx.HEART, handler);
        handler.addSprite(heart);

        int expectedHealth = Math.min((player.health + heart.getAmount()), 100);

        // heal player
        heart.addHealth(player);

        assertEquals(expectedHealth, player.health);
        assertFalse(handler.getSprites().contains(heart));
    }

    @Test
    public void playerAddsEveryAmmo() {
        Player player = new Player(0, 0, Gfx.PLAYER, handler);
        handler.addSprite(player);
        AirAmmo airAmmo = new AirAmmo(3, 0, Gfx.AIR_AMMO, handler);
        handler.addSprite(airAmmo);
        EarthAmmo earthAmmo = new EarthAmmo(6, 0, Gfx.EARTH_AMMO, handler);
        handler.addSprite(earthAmmo);
        WaterAmmo waterAmmo = new WaterAmmo(9, 0, Gfx.WATER_AMMO, handler);
        handler.addSprite(waterAmmo);
        FireAmmo fireAmmo = new FireAmmo(12, 0, Gfx.FIRE_AMMO, handler);
        handler.addSprite(fireAmmo);

        PlayerControl control = new PlayerControl(player);
        ArrayList<String> keyHandler = new ArrayList<>();
        keyHandler.add("D");

        int expectedAirAmmo = player.airAmmo + airAmmo.getAmount();
        int expectedEarthAmmo = player.earthAmmo + earthAmmo.getAmount();
        int expectedWaterAmmo = player.waterAmmo + waterAmmo.getAmount();
        int expectedFireAmmo = player.fireAmmo + fireAmmo.getAmount();

        // move and add ammo
        control.movement(keyHandler);
        handler.update(mock(Camera.class));
        assertEquals(3, player.getX());
        airAmmo.addAmmo(player);
        assertFalse(handler.getSprites().contains(airAmmo));
        assertEquals(expectedAirAmmo, player.airAmmo);

        control.movement(keyHandler);
        handler.update(mock(Camera.class));
        assertEquals(6, player.getX());
        earthAmmo.addAmmo(player);
        assertFalse(handler.getSprites().contains(earthAmmo));
        assertEquals(expectedEarthAmmo, player.earthAmmo);

        control.movement(keyHandler);
        handler.update(mock(Camera.class));
        assertEquals(9, player.getX());
        waterAmmo.addAmmo(player);
        assertFalse(handler.getSprites().contains(waterAmmo));
        assertEquals(expectedWaterAmmo, player.waterAmmo);


        control.movement(keyHandler);
        handler.update(mock(Camera.class));
        assertEquals(12, player.getX());
        fireAmmo.addAmmo(player);
        assertFalse(handler.getSprites().contains(fireAmmo));
        assertEquals(expectedFireAmmo, player.fireAmmo);
    }
}